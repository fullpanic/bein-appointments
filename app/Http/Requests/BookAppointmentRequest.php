<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\Definitions\Enums\EDate;

class BookAppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => [
                'bail', 'required', 'date', 'date_format:' . EDate::DATETIME,
                function ($attribute, $value, $fail) {
                    $date = Carbon::createFromFormat(EDate::DATETIME, $value);
                    if ($date->minute % 15 !== 0)
                        $fail('minutes must be a multiple of 15');
                }
            ],
            'to' => [
                'bail', 'required', 'date', 'date_format:' . EDate::DATETIME, 'after:from',
                function ($attribute, $value, $fail) {
                    $date = Carbon::createFromFormat(EDate::DATETIME, $value);
                    if ($date->minute % 15 !== 0)
                        $fail('minutes must be a multiple of 15');
                }
            ]
        ];
    }
}
