<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Requests\PaginationRequest;
use App\Expert;
use App\Appointment;
use App\Definitions\ExpertDefinition;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\ExpertResource;
use App\Http\Requests\ListAppointmentsRequest;
use App\Helpers\Timezone;
use App\Helpers\TimezoneConverter;
use App\Http\Requests\BookAppointmentRequest;
use App\Definitions\AppointmentDefinition;
use App\Http\Resources\AppointmentResource;
use Illuminate\Validation\ValidationException;

class ExpertController extends BaseController
{
    public function index(PaginationRequest $request)
    {
        $experts = Expert::paginate();
        
        return new PaginationResource(ExpertResource::class, $experts);
    }

    public function show(Expert $expert)
    {
        return $this->ok(new ExpertResource($expert));
    }

    public function appointments(ListAppointmentsRequest $request, Expert $expert)
    {
        $tz = Timezone::get();

        $from = TimezoneConverter::utc($request->get('from'), $tz);
        $to = TimezoneConverter::utc($request->get('to'), $tz);

        $appointments = $expert->appointments()
            ->where(AppointmentDefinition::FROM,  '>=', $from)
            ->where(AppointmentDefinition::TO, '<=', $to)
            ->get();

        return $this->ok(AppointmentResource::collection($appointments));
    }

    public function book(BookAppointmentRequest $request, Expert $expert)
    {
        $tz = Timezone::get();
        $from = TimezoneConverter::utc($request->get('from'), $tz);
        $to = TimezoneConverter::utc($request->get('to'), $tz);

        $bookedAppointents = $expert->appointments()
            ->where(function ($query) use ($from, $to) {
                $query->orWhere(function ($query) use ($from, $to) {
                    return $query->where(AppointmentDefinition::FROM, '<', $from)
                        ->where(AppointmentDefinition::TO, '>', $from);
                })
                ->orWhere(function ($query) use ($from, $to) {
                    return $query->where(AppointmentDefinition::FROM, '<', $to)
                        ->where(AppointmentDefinition::TO, '>', $to);
                })
                ->orWhere(function ($query) use ($from, $to) {
                    return $query->where(AppointmentDefinition::FROM, '>=', $from)
                        ->where(AppointmentDefinition::TO, '<=', $to);
                });
            })->get();
        
        if (count($bookedAppointents) > 0)
            throw ValidationException::withMessages([
                "Appointment already booked at the same time."
            ]);

        $appointment = Appointment::create([
            AppointmentDefinition::EXPERT_ID => $expert[ExpertDefinition::ID],
            AppointmentDefinition::FROM => TimezoneConverter::utc($request->get('from'), $tz),
            AppointmentDefinition::TO => TimezoneConverter::utc($request->get('to'), $tz),
        ]);
        return $this->ok(new AppointmentResource($appointment));
    }
}
