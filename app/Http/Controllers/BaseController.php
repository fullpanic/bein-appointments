<?php

namespace App\Http\Controllers;

use App\Http\Resources\Wrappers\Response;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function ok($data, $message = null, $errors = null)
    {
        return response()->json(new Response($data, $message, $errors), 200, []);
    }
    
    public function badRequest($message)
    {
        return response()->json(new Response(null, $message), 400);
    }

    public function forbidden($message)
    {
        return response()->json(new Response(null, $message), 403);
    }

    public function notFound($message = "The requested resource was not found.")
    {
        return response()->json(new Response(null, $message), 404);
    }

    public function internalError($message = "Sorry, Something wrong happened on our side, please try again in a few moments.")
    {
        return response()->json(new Response(null, $message), 500);
    }

    public function deleted($message = 'Resource deleted.')
    {
        return $this->ok(null, $message);
    }
}
