<?php

namespace App\Http\Resources\Wrappers;

use JsonSerializable;

class Response implements JsonSerializable
{
    public $data;
    public $message;
    public $errors;

    public function __construct($data = null, $message = null, $errors = null)
    {
        $this->data = $data;
        $this->message = $message;
        $this->errors = $errors;
    }

    public function jsonSerialize()
    {
        $returnValue = [];

        if ($this->data)
            $returnValue['data'] = $this->data;

        if ($this->message)
            $returnValue['message'] = $this->message;

        if($this->errors)
            $returnValue['errors'] = $this->errors;

        return $returnValue;
    }
}
