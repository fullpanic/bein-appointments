<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Helpers\Timezone;
use App\Helpers\TimezoneConverter;
use App\Definitions\Enums\EDate;
use App\Definitions\AppointmentDefinition;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tz = Timezone::get();
        return [
            'id' => $this[AppointmentDefinition::ID],
            'from' => TimezoneConverter::local($this[AppointmentDefinition::FROM], $tz),
            'to' => TimezoneConverter::local($this[AppointmentDefinition::TO], $tz)
        ];
    }
}
