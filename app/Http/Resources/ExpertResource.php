<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Definitions\ExpertDefinition;
use Carbon\Carbon;
use App\Helpers\TimezoneConverter;
use App\Helpers\Timezone;
use App\Definitions\Enums\EDate;

class ExpertResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $workStart = Carbon::createFromFormat(EDate::TIME_SHORT, $this[ExpertDefinition::WORKING_HOURS_START]);
        $workStart = $workStart->format(EDate::DATETIME);
        $workStart = TimezoneConverter::utc($workStart, $this[ExpertDefinition::TIMEZONE]);
        $workStart = TimezoneConverter::local($workStart, Timezone::get());
        $workStart = Carbon::createFromFormat(EDate::DATETIME, $workStart);
        $workStart = $workStart->format(EDate::TIME_SHORT);

        $workEnd = Carbon::createFromFormat(EDate::TIME_SHORT, $this[ExpertDefinition::WORKING_HOURS_END]);
        $workEnd = $workEnd->format(EDate::DATETIME);
        $workEnd = TimezoneConverter::utc($workEnd, $this[ExpertDefinition::TIMEZONE]);
        $workEnd = TimezoneConverter::local($workEnd, Timezone::get());
        $workEnd = Carbon::createFromFormat(EDate::DATETIME, $workEnd);
        $workEnd = $workEnd->format(EDate::TIME_SHORT);

        return [
            'id' => $this[ExpertDefinition::ID],
            'name' => $this[ExpertDefinition::NAME],
            'expertise' => $this[ExpertDefinition::EXPERTISE],
            'country' => $this[ExpertDefinition::COUNTRY],
            'timezone' => $this[ExpertDefinition::TIMEZONE],
            'working_hours_start' => $workStart,
            'working_hours_end' => $workEnd
        ];
    }
}
