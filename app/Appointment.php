<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Definitions\AppointmentDefinition;

class Appointment extends Model
{
    protected $table = AppointmentDefinition::TABLE;

    protected $fillable = AppointmentDefinition::FILLABLE;

    public function expert()
    {
        return $this->belongsTo(Expert::class);
    }
}
