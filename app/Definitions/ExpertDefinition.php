<?php

namespace App\Definitions;

class ExpertDefinition
{   
    const TABLE = 'experts';

    const ID = 'id';
    const NAME = 'name';
    const EXPERTISE = 'expertise';
    const COUNTRY = 'country';
    const TIMEZONE = 'timezone';
    const WORKING_HOURS_START = 'working_hours_start';
    const WORKING_HOURS_END = 'working_hours_end';

    const FILLABLE = [
        self::NAME,
        self::EXPERTISE,
        self::COUNTRY,
        self::TIMEZONE,
        self::WORKING_HOURS_START,
        self::WORKING_HOURS_END
    ];

}