<?php

namespace App\Definitions;

class AppointmentDefinition
{   
    const TABLE = 'appointments';

    const ID = 'id';
    const FROM = 'from';
    const TO = 'to';
    const EXPERT_ID = 'expert_id';

    const FILLABLE = [
        self::FROM,
        self::TO,
        self::EXPERT_ID
    ];

}