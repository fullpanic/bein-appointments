<?php

namespace App\Definitions\Enums;

class EDate
{   
    const DATETIME = 'Y-m-d H:i:s';
    const DATE = 'Y-m-d';
    const TIME = 'H:i:s';
    const TIME_SHORT = 'H:i';
}