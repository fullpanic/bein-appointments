<?php


namespace App\Helpers;

use Carbon\Carbon;
use Carbon\CarbonTimeZone;

class Timezone
{
    const FREEGEOIP_ENDPOINT = 'http://freegeoip.app/json/';

    const TIMEZONE_HEADER_KEY = 'X-Timezone';

    public static function fromIP($ip = null)
    {
        $ip = $ip ?? request()->ip();

        if ($ip === '127.0.0.1' || $ip === 'localhost')
            throw new \Exception('Local IP detected, unable to get timezone.');

        $result = json_decode(file_get_contents(self::FREEGEOIP_ENDPOINT . $ip));
        return $result->time_zone;
    }

    public static function get()
    {
        if ($tz = request()->header(self::TIMEZONE_HEADER_KEY))
            return $tz;
        return self::fromIP();
    }
}
