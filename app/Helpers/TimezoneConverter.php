<?php


namespace App\Helpers;

use Carbon\Carbon;
use App\Definitions\Enums\EDate;

class TimezoneConverter
{
    const UTC = 'UTC';

    public static function utc(string $datetime, $tz)
    {
        $datetime = Carbon::createFromFormat(EDate::DATETIME, $datetime, $tz);
        $datetime->setTimezone(self::UTC);
        return $datetime->format(EDate::DATETIME);
    }

    public static function local(string $datetime, $tz)
    {
        $datetime = Carbon::createFromFormat(EDate::DATETIME, $datetime, self::UTC);
        $datetime->setTimezone($tz);
        return $datetime->format(EDate::DATETIME);
    }
}
