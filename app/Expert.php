<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Definitions\ExpertDefinition;

class Expert extends Model
{
    protected $table = ExpertDefinition::TABLE;

    protected $fillable = ExpertDefinition::FILLABLE;

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
}
