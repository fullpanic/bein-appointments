<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Definitions\AppointmentDefinition;
use App\Definitions\ExpertDefinition;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AppointmentDefinition::TABLE, function (Blueprint $table) {
            $table->id(AppointmentDefinition::ID);
            $table->datetime(AppointmentDefinition::FROM);
            $table->datetime(AppointmentDefinition::TO);
            $table->unsignedBigInteger(AppointmentDefinition::EXPERT_ID);
            $table->foreign(AppointmentDefinition::EXPERT_ID)
                ->references(ExpertDefinition::ID)
                ->on(ExpertDefinition::TABLE);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AppointmentDefinition::TABLE);
    }
}
