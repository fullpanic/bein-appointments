<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Definitions\ExpertDefinition;

class CreateExpertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ExpertDefinition::TABLE, function (Blueprint $table) {
            $table->id(ExpertDefinition::ID);
            $table->string(ExpertDefinition::NAME);
            $table->string(ExpertDefinition::EXPERTISE);
            $table->string(ExpertDefinition::COUNTRY);
            $table->string(ExpertDefinition::TIMEZONE);
            $table->string(ExpertDefinition::WORKING_HOURS_START, 5);
            $table->string(ExpertDefinition::WORKING_HOURS_END, 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ExpertDefinition::TABLE);
    }
}
