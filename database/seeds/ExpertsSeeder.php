<?php

use Illuminate\Database\Seeder;
use App\Expert;
use App\Definitions\ExpertDefinition;

class ExpertsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Expert::create([
            ExpertDefinition::NAME => 'William Jordan',
            ExpertDefinition::EXPERTISE => 'Doctor',
            ExpertDefinition::COUNTRY => 'Anabar',
            ExpertDefinition::TIMEZONE => 'NZST',
            ExpertDefinition::WORKING_HOURS_START => '06:00',
            ExpertDefinition::WORKING_HOURS_END => '17:00'
        ]);
        Expert::create([
            ExpertDefinition::NAME => 'Quasi Shawa',
            ExpertDefinition::EXPERTISE => 'Civil Engineer',
            ExpertDefinition::COUNTRY => 'Syria',
            ExpertDefinition::TIMEZONE => 'EEST',
            ExpertDefinition::WORKING_HOURS_START => '06:00',
            ExpertDefinition::WORKING_HOURS_END => '12:00'
        ]);
        Expert::create([
            ExpertDefinition::NAME => 'Shimaa Badawy',
            ExpertDefinition::EXPERTISE => 'Computer Engineer',
            ExpertDefinition::COUNTRY => 'Egypt',
            ExpertDefinition::TIMEZONE => 'EET',
            ExpertDefinition::WORKING_HOURS_START => '13:00',
            ExpertDefinition::WORKING_HOURS_END => '14:00'
        ]);
    }
}
